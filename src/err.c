/*
    Copyright (C) 2001-2015 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#ifdef HAVE_LIMITS_H
#include <limits.h>
#ifndef LINE_MAX
#ifdef _POSIX2_LINE_MAX
#define LINE_MAX _POSIX2_LINE_MAX
#else
#define LINE_MAX 2048
#endif
#endif
#endif

#define err(eval, fmt, ...)	print_string(eval, 1, fmt, ## __VA_ARGS__)
#define errx(eval, fmt, ...)	print_string(eval, 0, fmt, ## __VA_ARGS__)
#define warn(fmt, ...)		print_string(-1, 0, fmt, ## __VA_ARGS__)
#define warnx(fmt, ...)		print_string(-1, 1, fmt, ## __VA_ARGS__)

void print_string(int eval, int prog, const char *fmt, ...)
{
    va_list ap;
    char line[LINE_MAX];

#ifndef HAVE___PROGNAME
    char *__progname = "ui";
#else
    extern char *__progname;
#endif

    va_start(ap, fmt);
    vsnprintf(line, sizeof(line), fmt, ap);
    va_end(ap);

    fprintf(stderr, "%s%s%s\n", (prog) ? __progname : "", (prog) ? ": " : "",
	    line);

    if (eval > -1)
	exit(eval);
}
